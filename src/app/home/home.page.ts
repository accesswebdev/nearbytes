import { Component } from '@angular/core';
declare var NearBytes: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor() {}
  ngOnInit() {
    this.loadNearBytes();
  }
  loadNearBytes()
  {
     var mNearBytesListener={
        onReceiveData:function(e){
          console.log('-->onReceiveData<--', e);
          NearBytes.bytesToString( e , function( value ){
              console.log('bytesToString', e)
          });
        },
        onReceiveError:function(e){
          console.log('onReceiveError', e)
        },
    };

    let mNearBytesStarter = function( success )
    {
        console.log('mNearBytesStarter', success);
        if(success)
        {
          NearBytes.debugMode();
          NearBytes.setNearBytesListener(mNearBytesListener);
          NearBytes.startListening();
        }
    }

    NearBytes.create( { oncomplete:mNearBytesStarter, appkey: '32fa13e509018507f4ec3fe717a55b3fb18cedb6ee43bc11929283b56656ae0065c2eab5e885ad6a535c4762ea92d8cd', appid:'1937' } );
  }

}
